# coding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll-poly/version'

Gem::Specification.new do |spec|
  spec.name          = "jekyll-poly"
  spec.version       = Jekyll::Poly::VERSION
  spec.authors       = ["Leo Blondel", "Lisa Blondel"]
  spec.email         = ["leo@jogl.io", "blondel.lisa@gmail.com"]

  spec.summary       = %q{jekyll plugin to generate html snippets for embedding Google Poly Assets}
  spec.description   = %q{jekyll plugin to generate html snippets for embedding Google Poly Assets}
  spec.homepage      = "https://gitlab.com/xqua/jekyll-poly"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'jekyll'
  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
end
