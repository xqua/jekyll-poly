require "jekyll"
require "jekyll-poly/version"
class PolyEmbed < Liquid::Tag

  def initialize(tagName, content, tokens)
    super
    @content = content
  end

  public

  def generate_iframe(poly_id)
    %Q{<style>.embed-container {  position: relative;  padding-bottom: 56.25%;  height: 0;  overflow: hidden;  max-width: 100%;}.embed-container iframe,.embed-container object,.embed-container embed {  position: absolute;  top: 0;  left: 0;  width: 100%;  height: 100%;}</style><div class=".embed-container"><iframe width="100%" height="480px" src="https://poly.google.com/view/#{ poly_id }/embed" frameborder="0" style="border:none;" allowvr="yes" allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel="" ></iframe></div>}
  end

  def generate_from_template(context, tmpl_path, poly_id)
    @poly_id = poly_id
    tmpl = File.read tmpl_path
    site = context.registers[:site]
    tmpl = (Liquid::Template.parse tmpl).render site.site_payload.merge!({"poly_id" => @poly_id})
  end

  def generate(context, poly_id)
    tmpl_path = File.join Dir.pwd, "_includes", "poly.html"
    if File.exist? tmpl_path
      generate_from_template context, tmpl_path, poly_id
    else
      generate_iframe poly_id
    end
  end

  def render(context)
    poly_url = "#{context[@content.strip]}"

    if poly_url[/poly\.google\.com\/view\/([^\?]*)/]
      poly_id = $1
    else
      raise "The URL provided #{ poly_url } does not match a google poly URL"
    end

    generate(context, poly_id)
  end
  Liquid::Template.register_tag "google_poly", self
end
